#ifndef LAB1_INCLUDE_H
#define LAB1_INCLUDE_H

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <time.h>

#include "utils.h"

#include "reiserfs_struct.h"
#include "reiserfs_lib.h"

void init_read_block(size_t capacity);
struct read_blocks *find_in_read_blocks_on_number(size_t block_numb);
void *block_read(FILE *fp, size_t block_count, size_t block_size);
void free_read_block();

struct directories *find_dir_on_key(struct directories *head, struct reiserfs_key *key);
struct directories *find_dir_on_name(struct directories *head, char *name);
int cmp_keys(struct reiserfs_key *key1, struct reiserfs_key *key2);

void openFS(char *file_name);
void preSetup() ;
void clearFS();

void moveDirectory(char *new);
void copyToHost(char *name, char *fn);
char *getCurrentDirName();

#endif //LAB1_INCLUDE_H

#ifndef LAB1_REISERFS_LIB_H
#define LAB1_REISERFS_LIB_H

#include "reiserfs_struct.h"

int is_blocksize_correct(unsigned int blocksize);
int is_reiserfs_3_5_magic_string(struct reiserfs_super_block *rs);
int is_reiserfs_3_6_magic_string(struct reiserfs_super_block *rs);
int is_reiserfs_jr_magic_string(struct reiserfs_super_block *rs);
int does_look_like_super_block(struct reiserfs_super_block *sb);
int is_any_reiserfs_magic_string(struct reiserfs_super_block *rs);
//int get_reiserfs_format(struct reiserfs_super_block *sb);
//int reiserfs_super_block_size(struct reiserfs_super_block *rs);
/*int magic_2_version (struct reiserfs_super_block * rs);*/
//int is_prejournaled_reiserfs(struct reiserfs_super_block *rs);
int who_is_this(const char *buf, int blocksize);
int leaf_count_ih(const char *buf, int blocksize);
int leaf_free_space_estimate(const char *buf, int blocksize);
int is_a_leaf(const char *buf, int blocksize);
//int leaf_item_number_estimate(const struct buffer_head *bh);
int is_correct_internal(const char *buf, int blocksize);
int is_desc_block(const char *buf, unsigned long buf_size);
int get_type(const struct reiserfs_key *key);
u_int32_t type2uniqueness(int type);
int uniqueness2type(u_int32_t uniqueness);
size_t get_offset(const struct reiserfs_key *key);
int key_format(const struct reiserfs_key *key);
char *key_of_what(const struct reiserfs_key *key);
int leaf_item_number_estimate(const char *bh, int blocksize);

#endif //LAB1_REISERFS_LIB_H

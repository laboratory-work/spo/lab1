#ifndef LAB1_UTILS_H
#define LAB1_UTILS_H

void die(const char *fmt, ...);
char **str_split(char *a_str, const char a_delim, int *countElement);
char *trim(char *str);
char *trim1(char *str, const char *sep);

void *calloc_int(size_t nmemb, size_t size);
void free_int(void *ptr);
void *realloc_int(void *ptr, size_t size);

#endif //LAB1_UTILS_H

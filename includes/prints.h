#ifndef LAB1_PRINTS_H
#define LAB1_PRINTS_H

#include "include.h"

void printDirs(struct directories *dir, int level);
char *typeFile2name(int mode);
void printCurrentDirs();

void printSuperBlock(struct reiserfs_super_block *);
void printKey(struct reiserfs_key *);
void printBlocHead(struct block_head *);
void printDiskChild(struct disk_child *);
void printItemHead(struct item_head *);

void printStatData1(struct stat_data_v1 *sd1);
void printStatData(struct stat_data *sd);
void printDirHead(struct reiserfs_de_head *pHead);

#endif //LAB1_PRINTS_H

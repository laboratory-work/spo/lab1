#ifndef LAB1_INCLUDE_MAIN_H
#define LAB1_INCLUDE_MAIN_H

#ifndef PART_1
    #define PART_1 "--list"
#endif

#ifndef PART_2
    #define PART_2 "--work"
#endif

int part_1();
int part_2(char *argv);

#endif //LAB1_INCLUDE_MAIN_H

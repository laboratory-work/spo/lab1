#ifndef LAB1_PART2_INTERACT_H
#define LAB1_PART2_INTERACT_H

#include "../includes/include.h"
#include "../includes/prints.h"

#ifndef MAX_LEN_CLI
    #define MAX_LEN_CLI 512
#endif

typedef int bool;
typedef bool (*cmdFuncImpl)(char **args);

#define true (0 == 0)
#define false (0 != 0)

typedef struct {
    char* cmdName;
    int argsCount;
    cmdFuncImpl func;
} commandInf;

void interactive_new();

#endif //LAB1_PART2_INTERACT_H

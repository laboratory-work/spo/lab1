#ifndef LAB1_REISERFS_STRUCT_H
#define LAB1_REISERFS_STRUCT_H

#include <stdlib.h>

/* in reading the #defines, it may help to understand that they employ the
   following abbreviations:

   B = Buffer
   I = Item header
   H = Height within the tree (should be changed to LEV)
   N = Number of the item in the node
   STAT = stat data
   DEH = Directory Entry Header
   EC = Entry Count
   E = Entry number
   UL = Unsigned Long
   BLKH = BLocK Header
   UNFM = UNForMatted node
   DC = Disk Child
   P = Path

   These #defines are named by concatenating these abbreviations, where first
   comes the arguments, and last comes the return value, of the macro.
*/

/** **/
#define DEFAULT_BLOCKSIZE 4096

/*
 * ReiserFS leaves the first 64k unused, so that partition labels have enough space.
 * If someone wants to write a fancy bootloader that needs more than 64k, let us know,
 * and this will be increased in size. This number must be larger than than the
 * largest block size on any platform, or code will break.
 */
#define REISERFS_DISK_OFFSET_IN_BYTES (16 * 4 * 1024)

/* various reiserfs_1 signatures. We have 3 so far. ReIsErFs for 3.5 format with standard
 * journal, ReIsEr2Fs for 3.6 (or converted 3.5) and ReIsEr3Fs for filesystem with non-standard
 * journal (formats are distinguished by sb_version in that case). Those signatures should be
 * looked for at the 64-th and at the 8-th 1k block of the device
 */
#define REISERFS_3_5_SUPER_MAGIC_STRING "ReIsErFs"
#define REISERFS_3_6_SUPER_MAGIC_STRING "ReIsEr2Fs"
/* JR stands for Journal Relocation */
#define REISERFS_JR_SUPER_MAGIC_STRING "ReIsEr3Fs"
/* ick.  magic string to find desc blocks in the journal */
#define JOURNAL_DESC_MAGIC "ReIsErLB"

//#define SB_SIZE_V1 (sizeof(struct reiserfs_super_block_v1)) /* 76 bytes */
//#define SB_SIZE (sizeof(struct reiserfs_super_block)) /* 204 bytes */
#define IH_SIZE (sizeof(struct item_head))
#define BLKH_SIZE (sizeof(struct block_head))
#define SD_V1_SIZE (sizeof(struct stat_data_v1))
#define SD_SIZE (sizeof(struct stat_data)) /* this is 44 bytes long */
#define DEH_SIZE sizeof(struct reiserfs_de_head)
#define DC_SIZE (sizeof(struct disk_child))
#define INDIRNOD_SIZE (sizeof(struct indirect_node))
#define DIRNOD_SIZE (sizeof(struct direct_node))
#define DIR_SIZE (sizeof(struct directories))
#define KEY_SIZE (sizeof(struct reiserfs_key))
#define ITM_SIZE sizeof(struct items)

#define REISERFS_ROOT_OBJECTID 2
#define REISERFS_ROOT_PARENT_OBJECTID 1

// values for k_type field of the struct reiserfs_key
#define TYPE_STAT_DATA 0
#define TYPE_INDIRECT 1
#define TYPE_DIRECT 2
#define TYPE_DIRENTRY 3
#define TYPE_MAXTYPE 4
#define TYPE_UNKNOWN 15
/* special type for symlink not conflicting to any of item types. */
#define TYPE_SYMLINK    4

#define KEY_FORMAT_1 0
#define KEY_FORMAT_2 1
#define KEY_FORMAT_UNDEFINED 15

/* Node of this level is out of the tree. */
#define FREE_LEVEL        0
/* Leaf node level.                       */
#define DISK_LEAF_NODE_LEVEL  1

#define V1_SD_UNIQUENESS 0
#define V1_DIRENTRY_UNIQUENESS 500
#define DIRENTRY_UNIQUENESS 500
#define V1_DIRECT_UNIQUENESS 0xffffffff
#define V1_INDIRECT_UNIQUENESS 0xfffffffe
#define V1_UNKNOWN_UNIQUENESS 555

#define THE_LEAF 1
#define THE_INTERNAL 2
#define THE_SUPER 3
#define THE_JDESC 4
#define HAS_IH_ARRAY 5
#define THE_UNKNOWN 6

#define S_MODE 0x0FFF

#define MAX_ITEM_LEN(block_size) (block_size - BLKH_SIZE - IH_SIZE)
#define MIN_ITEM_LEN 1
#define MAX_HEIGHT 6

/***************************************************************************/
/*                             SUPER BLOCK                                 */
/***************************************************************************/

/* super block of prejournalled version */
struct reiserfs_super_block_v0 {
    u_int32_t s_block_count;
    u_int32_t s_free_blocks;
    u_int32_t s_root_block;
    u_int16_t s_blocksize;
    u_int16_t s_oid_maxsize;
    u_int16_t s_oid_cursize;
    u_int16_t s_state;
    char s_magic[16];
    u_int16_t s_tree_height;
    u_int16_t s_bmap_nr;
    u_int16_t s_reserved;
};

struct journal_params {
    /* where does journal start from on its device */
    u_int32_t jp_journal_1st_block;
    /* journal device st_rdev */
    u_int32_t jp_journal_dev;
    /* size of the journal on FS creation. used to make sure they don't overflow it */
    u_int32_t jp_journal_size;
    /* max number of blocks in a transaction.  */
    u_int32_t jp_journal_trans_max;
    /* random value made on fs creation (this was sb_journal_block_count) */
    u_int32_t jp_journal_magic;
    /* max number of blocks to batch into a trans */
    u_int32_t jp_journal_max_batch;
    /* in seconds, how old can an async commit be */
    u_int32_t jp_journal_max_commit_age;
    /* in seconds, how old can a transaction be */
    u_int32_t jp_journal_max_trans_age;
};

/* this is the super from 3.5.X */
struct reiserfs_super_block_v1 {
    /* number of block on data device */
    u_int32_t sb_block_count;
    /* free blocks count */
    u_int32_t sb_free_blocks;
    /* root of the tree */
    u_int32_t sb_root_block;

    struct journal_params sb_journal;

    u_int16_t sb_blocksize;
    /* max size of object id array, see get_objectid() commentary */
    u_int16_t sb_oid_maxsize;
    /* current size of object id array */
    u_int16_t sb_oid_cursize;
    /* this is set to 1 when filesystem was umounted, to 2 - when not */
    u_int16_t sb_umount_state;

    /*
     * reiserfs_1 magic string indicates that file system
     * is reiserfs_1: "ReIsErFs" or "ReIsEr2Fs" or "ReIsEr3Fs"
     */
    char s_magic[10];

    /*
     * it is set to used by fsck to mark which phase of
     * rebuilding is done (used for fsck debugging)
     */
    u_int16_t sb_fs_state;

    /*
     * code of fuction which was/is/will be
     * used to sort names in a directory. See codes in above
     */
    u_int32_t sb_hash_function_code;

    /*
     * height of filesytem tree. Tree
     * consisting of only one root block has 2 here
     */
    u_int16_t sb_tree_height;

    /*
     * amount of bitmap blocks needed to
     * address each block of file system
     */
    u_int16_t sb_bmap_nr;

    /*
     * this field is only reliable on
     * filesystem with non-standard journal
     */
    u_int16_t sb_version;

    /*
     * size in blocks of journal area on
     * main device, we need to keep after
     * non-standard journal relocation
     */
    u_int16_t sb_reserved_for_journal;
};

/* Structure of super block on disk */
struct reiserfs_super_block {
    struct reiserfs_super_block_v1 s_v1;
    u_int32_t sb_inode_generation;
    u_int32_t s_flags;
    /* Right now used only by inode-attributes, if enabled */
    unsigned char s_uuid[16];
    /* filesystem unique identifier */
    char s_label[16];
    /* filesystem volume label */
    u_int16_t s_mnt_count;
    u_int16_t s_max_mnt_count;
    u_int32_t s_lastcheck;
    u_int32_t s_check_interval;
    char s_unused[76];
} __attribute__ ((__packed__));

/***************************************************************************/
/*                             JOURNAL                                     */
/***************************************************************************/

//#define JOURNAL_TRANS_HALF 1018   /* must be correct to keep the desc and commit structs at 4k */

/* first block written in a commit.  BUG, not 64bit safe */
struct reiserfs_journal_desc {
    /* id of commit */
    u_int32_t j2_trans_id;
    /* length of commit. len +1 is the commit block */
    u_int32_t j2_len;
    /* mount id of this trans */
    u_int32_t j2_mount_id;
    /* real locations for each block */
    u_int32_t j2_realblock[1];
};

/***************************************************************************/
/*                       KEY & ITEM HEAD                                   */
/***************************************************************************/

struct offset_v1 {
    u_int32_t k_offset;
    u_int32_t k_uniqueness;
} __attribute__ ((__packed__));

/*
 * little endian structure:
 * bits 0-59 [60]: offset
 * bits 60-63 [4]: type
 */
struct offset_v2 {
    u_int64_t v;
} __attribute__ ((__packed__));

/* Key of the object determines object's location in the tree, composed of 4 components */
struct reiserfs_key {
    /* packing locality: by default parent directory object id */
    u_int32_t k2_dir_id;
    /* object identifier */
    u_int32_t k2_objectid;
    union {
        struct offset_v1 k2_offset_v1;
        struct offset_v2 k2_offset_v2;
    } __attribute__ ((__packed__)) u;
} __attribute__ ((__packed__));

struct item_head {
    /* Everything in the tree is found by searching for it based on its key. */
    struct reiserfs_key ih_key;
    union {
        /*
         * The free space in the last unformatted node of an indirect item if this is an indirect
         * item. This equals 0xFFFF iff this is a direct item or stat data item. Note that the key, not
         * this field, is used to determine the item type, and thus which field this union contains.
         */
        u_int16_t ih2_free_space;
        /* Iff this is a directory item, this field equals the number of directory entries in the directory item. */
        u_int16_t ih2_entry_count;
    } __attribute__ ((__packed__)) u;
    /* total size of the item body */
    u_int16_t ih2_item_len;
    /* an offset to the item body within the block */
    u_int16_t ih2_item_location;
    /* key format is stored in bits 0-11 of this item flags are stored in bits 12-15 */
    u_int16_t ih_format;
} __attribute__ ((__packed__));

/*
 * Picture represents a leaf of internal tree
 *  ______________________________________________________
 * |      |  Array of     |                   |           |
 * |Block |  Object-Item  |      F r e e      |  Objects- |
 * | head |  Headers      |     S p a c e     |   Items   |
 * |______|_______________|___________________|___________|
 */

/*
 * Header of a disk block.  More precisely, header of a formatted leaf
 * or internal node, and not the header of an unformatted node.
 */
struct block_head {
    /* Level of a block in the tree. */
    u_int16_t blk2_level;
    /* Number of keys/items in a block. */
    u_int16_t blk2_nr_item;
    /* Block free space in bytes. */
    u_int16_t blk2_free_space;
    u_int16_t blk_reserved;
    u_int32_t reserved[4];
};

/***************************************************************************/
/*                             STAT DATA                                   */
/***************************************************************************/

/* Stat Data on disk (reiserfs_1 version of UFS disk inode minus the address blocks) */

/* The sense of adding union to stat data is to keep a value of real number of blocks used by file.
 * The necessity of adding such information is caused by existing of files with holes.
 * Reiserfs should keep number of used blocks for file, but not calculate it from file size
 * (that is not correct for holed files). Thus we have to add additional information to stat data.
 * When we have a device special file, there is no need to get number of used blocks for them, and,
 * accordingly, we doesn't need to keep major and minor numbers for regular files, which might
 * have holes. So this field is being overloaded.
 */

struct stat_data_v1 {
    /* file type, permissions */
    u_int16_t sd_mode;
    /* number of hard links */
    u_int16_t sd_nlink;
    /* owner */
    u_int16_t sd_uid;
    /* group */
    u_int16_t sd_gid;
    /* file size */
    u_int32_t sd_size;
    /* time of last access */
    u_int32_t sd_atime;
    /* time file was last modified  */
    u_int32_t sd_mtime;
    /* time inode (stat data) was last changed (except changes to sd_atime and sd_mtime) */
    u_int32_t sd_ctime;
    union {
        u_int32_t sd_rdev;
        /* number of blocks file uses */
        u_int32_t sd_blocks;
    } __attribute__ ((__packed__)) u;
    /*
     * first byte of file which is stored in a direct item: except that if it equals 1 it is a symlink and if it
     * equals MAX_KEY_OFFSET there is no direct item. The existence of this field really grates on me.
     * Let's replace it with a macro based on sd_size and our tail suppression policy.  Someday
     */
    u_int32_t sd_first_direct_byte;
} __attribute__ ((__packed__));

/* Stat Data on disk (reiserfs_1 version of UFS disk inode minus the
   address blocks) */
struct stat_data {
    /* file type, permissions */
    u_int16_t sd_mode;
    u_int16_t sd_attrs;
    /* 32 bit nlink! */
    u_int32_t sd_nlink;
    /* 64 bit size! */
    u_int64_t sd_size;
    /* 32 bit uid! */
    u_int32_t sd_uid;
    /* 32 bit gid! */
    u_int32_t sd_gid;
    /* time of last access */
    u_int32_t sd_atime;
    /* time file was last modified  */
    u_int32_t sd_mtime;
    /* time inode (stat data) was last changed (except changes to sd_atime and sd_mtime) */
    u_int32_t sd_ctime;
    u_int32_t sd_blocks;
    union {
        u_int32_t sd_rdev;
        u_int32_t sd_generation;
    } __attribute__ ((__packed__)) u;
} __attribute__ ((__packed__));

/***************************************************************************/
/*                      DIRECTORY STRUCTURE                                */
/***************************************************************************/
/*
   Picture represents the structure of directory items
   ________________________________________________
   |  Array of     |   |     |        |       |   |
   | directory     |N-1| N-2 | ....   |   1st |0th|
   | entry headers |   |     |        |       |   |
   |_______________|___|_____|________|_______|___|
                    <----   directory entries         ------>

 First directory item has k_offset component 1. We store "." and ".." in one item, always,
 we never split "." and ".." into differing items. This makes, among other things, the code
 for removing directories simpler.
*/


/* Each directory entry has its header. This header has deh_dir_id and
   deh_objectid fields, those are key of object, entry points to */

struct reiserfs_de_head {
    /* third component of the directory entry key */
    u_int32_t deh2_offset;
    /* objectid of the parent directory of the object, that is referenced by directory entry */
    u_int32_t deh2_dir_id;
    /* objectid of the object, that is referenced by  directory entry */
    u_int32_t deh2_objectid;
    /* offset of name in the whole item */
    u_int16_t deh2_location;
    /* whether 1) entry contains stat data (for future), and 2) whether entry is hidden (unlinked) */
    u_int16_t deh2_state;
} __attribute__ ((__packed__));

/*
 * Picture represents an internal node of the reiserfs_1 tree
 *  ______________________________________________________
 * |      |  Array of     |  Array of         |  Free     |
 * |block |    keys       |  pointers         | space     |
 * | head |      N        |      N+1          |           |
 * |______|_______________|___________________|___________|
 */

/***************************************************************************/
/*                      DISK CHILD                                         */
/***************************************************************************/

/* Disk child pointer: The pointer from an internal node of the tree to a node that is on disk. */
struct disk_child {
    /* Disk child's block number. */
    u_int32_t dc2_block_number;
    /* Disk child's used space.   */
    u_int16_t dc2_size;
    u_int16_t dc2_reserved;
} __attribute__ ((__packed__));

/***************************************************************************/
/*                      INDERECT                                         */
/***************************************************************************/

struct indirect_node {
    u_int32_t **blocks;
    size_t *offsets;
    u_int32_t *len_in_block;
    size_t count;
};

struct direct_node {
    char *block;
    size_t length;
    size_t capacity;
};

struct directories {
    struct reiserfs_key *current;
    struct directories **child;
    int countChild;
    char *name;
    struct stat_data *stat_v2;
    struct stat_data_v1 *stat_v1;
    struct indirect_node *indirect;
    struct direct_node *direct;
    int length;
};


/***************************************************************************/
/*                      FUNCTIONS                                         */
/***************************************************************************/

#define set_key_uniqueness(k, val)    do { (k)->u.k2_offset_v1.k_uniqueness = (val); } while (0)
#define get_sb_block_size(sb)         (sb->s_v1.sb_blocksize)
#define get_key_offset_v1(k)         (k->u.k2_offset_v1.k_offset)
#define get_key_uniqueness(k)        (k->u.k2_offset_v1.k_uniqueness)
#define get_ih_item_len(ih)          (ih->ih2_item_len)
#define get_ih_location(ih)          (ih->ih2_item_location)
#define get_blkh_level(blkh)        ((blkh)->blk2_level)
#define get_blkh_nr_items(blkh)     ((blkh)->blk2_nr_item)
#define get_blkh_free_space(blkh)   ((blkh)->blk2_free_space)
#define is_leaf_block_head(buf) (get_blkh_level ((struct block_head *)(buf)) == DISK_LEAF_NODE_LEVEL)
#define is_internal_block_head(buf) \
((get_blkh_level (((struct block_head *)(buf))) > DISK_LEAF_NODE_LEVEL) &&\
 (get_blkh_level (((struct block_head *)(buf))) <= MAX_HEIGHT))

#define stat_data_v1(ih) (get_ih_key_format (ih) == KEY_FORMAT_1)
#define deh_offset(deh) ( ((deh)->deh_offset))
#define deh_dir_id(deh) ( ((deh)->deh_dir_id))
#define deh_objectid(deh) ( ((deh)->deh_objectid))
#define deh_location(deh) ( ((deh)->deh_location))
#define deh_state(deh) ( ((deh)->deh_state))

u_int16_t get_ih_key_format(const struct item_head *ih);

//#define KEY_IS_STAT_DATA_KEY(p_s_key)    ( get_type(p_s_key) == TYPE_STAT_DATA )
#define KEY_IS_DIRECTORY_KEY(p_s_key)    ( get_type(p_s_key) == TYPE_DIRENTRY )
#define KEY_IS_DIRECT_KEY(p_s_key)    ( get_type(p_s_key) == TYPE_DIRECT )
#define KEY_IS_INDIRECT_KEY(p_s_key)    ( get_type(p_s_key) == TYPE_INDIRECT )

//#define I_IS_STAT_DATA_ITEM(p_s_ih)    KEY_IS_STAT_DATA_KEY(&((p_s_ih)->ih_key))
//#define I_IS_DIRECTORY_ITEM(p_s_ih)    KEY_IS_DIRECTORY_KEY(&((p_s_ih)->ih_key))
//#define I_IS_DIRECT_ITEM(p_s_ih)    KEY_IS_DIRECT_KEY(&((p_s_ih)->ih_key))
//#define I_IS_INDIRECT_ITEM(p_s_ih)    KEY_IS_INDIRECT_KEY(&((p_s_ih)->ih_key))
//
//#define is_indirect_ih(ih) I_IS_INDIRECT_ITEM(ih)
//#define is_direct_ih(ih) I_IS_DIRECT_ITEM(ih)
//#define is_direntry_ih(ih) I_IS_DIRECTORY_ITEM(ih)
//#define is_stat_data_ih(ih) I_IS_STAT_DATA_ITEM(ih)

#define is_indirect_key(key) KEY_IS_INDIRECT_KEY(key)
#define is_direct_key(key) KEY_IS_DIRECT_KEY(key)
#define is_direntry_key(key) KEY_IS_DIRECTORY_KEY(key)
//#define is_stat_data_key(key) KEY_IS_STAT_DATA_KEY(key)

static inline u_int32_t get_key_type_v2(const struct reiserfs_key *key) {
    const struct offset_v2 *v2 = &key->u.k2_offset_v2;
    char type = v2->v >> 60;
    return (type <= TYPE_MAXTYPE) ? type : TYPE_UNKNOWN;
}

static inline u_int64_t get_key_offset_v2(const struct reiserfs_key *key) {
    const struct offset_v2 *v2 = &key->u.k2_offset_v2;
    return (v2->v) & (~0ULL >> 4);
}

#endif //LAB1_REISERFS_STRUCT_H

#ifndef LAB1_STACK_H
#define LAB1_STACK_H

#include <stdio.h>
#include <stdlib.h>

struct stack {
    int top;
    unsigned capacity;
    int *array;
};

struct stack *createStack(unsigned int capacity);
int isFull(struct stack *stack);
int isEmpty(struct stack *stack);
void push(struct stack *stack, int item);
int pop(struct stack *stack);
int peek(struct stack *stack);
void clean(struct stack *stack);

#endif //LAB1_STACK_H
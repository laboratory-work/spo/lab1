#include <stdio.h>
#include "../includes/part2_interact.h"

#define CI_SIZE(s) (sizeof(s)/sizeof(commandInf))

bool cmdNoCommand(char **args) {
    if(args == NULL) {
        fprintf(stderr, "Interrupt\n");
        return false;
    }else {
        fprintf(stderr, "Incorrect command: %s\n", args[0]);
        return true;
    }
}
bool cmdNoCom(char **args) {
    return true;
}

bool cmd_cd(char **args) {
    moveDirectory(args[1]);
    return true;
}

bool cmd_ls(char **args) {
    printCurrentDirs();
    return true;
}

bool cmd_cp(char **args) {
    copyToHost(args[1], args[2]);
    return true;
}

bool cmd_help(char **args) {
    fprintf(stdout, "Help:\n");
    fprintf(stdout, "\tcp <file> <host_file> \t Копирование\n");
    fprintf(stdout, "\tls \t Вывод содержания директории\n");
    fprintf(stdout, "\tcd <dir> \t Перемещение директории\n");
    fprintf(stdout, "\thelp \t Вывод помощи\n");
    fprintf(stdout, "\texit \t Выход\n");
    return true;
}

bool cmd_exit(char **args) {
    fprintf(stdout, "GoodBye!\n");
    return false;
}

commandInf knownCommands[] = {
        { "none", 0, &cmdNoCom },
        { "none", 0, &cmdNoCommand },
        { "cd", 1, &cmd_cd },
        { "cp", 2, &cmd_cp },
        { "ls", 0, &cmd_ls },
        { "help", 0, &cmd_help },
        { "exit", 0, &cmd_exit }
};

bool tryGetCmd(char *name, int argsCount, cmdFuncImpl *result) {
    *result = NULL;
    for (int i = 2; *result == NULL && i < CI_SIZE(knownCommands); i++) {
        commandInf cmd = knownCommands[i];
        if (strcmp(name, cmd.cmdName) == 0 && argsCount == cmd.argsCount) {
            *result = cmd.func;
        }
    }
    return *result != NULL;
}

void interactive_new() {
    fprintf(stdout, "Welcome to the reader ReiserFS! \n");
    char *s = calloc(MAX_LEN_CLI, sizeof(char));
    cmdFuncImpl currCmd;
    char **t = NULL;
    do {
        fprintf(stdout, "%s > ", getCurrentDirName());
        fgets(s, MAX_LEN_CLI, stdin);
        trim(s);

        if(s[0] == '\0') {
            fprintf(stdout, "\n");
            currCmd = cmdNoCom;
            continue;
        }

        if ((strlen(s) > 0) && (s[strlen(s) - 1] == '\n')) {  s[strlen(s) - 1] = '\0'; }

        if(t) { free_int(t); }

        int argsCount = 0;
        t = str_split(s, ' ', &argsCount);
        argsCount--;

        if (!tryGetCmd(t[0], argsCount, &currCmd)) {
            currCmd = cmdNoCommand;
        }
    } while(currCmd == NULL || currCmd(t));

    free_int(s);
    if(t) { free_int(t); }
}
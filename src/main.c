#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "../includes/include_main.h"

int main(int argc, char **argv) {
    if (argc == 1) {
        fprintf(stderr, "Please use program with argument --list or --work <file/device>.\n");
        return EAGAIN;
    }

    if (argc > 3) {
        fprintf(stderr, "Error. Many argument's\n");
        return E2BIG;
    }

    if (argc == 2) {
        if(strcmp(argv[1], PART_1) == 0) {
            part_1(argc, argv);
            return 0;
        }else if(strcmp(argv[1], PART_2) == 0) {
            fprintf(stderr, "Argument '%s' need a disk or file on next argument\n", argv[1]);
            return EAGAIN;
        }
    }else if(argc == 3 && strcmp(argv[1], PART_2) == 0) {
        part_2(argv[2]);
        return 0;
    }else {
        fprintf(stderr, "Argument '%s' not supported\n", argv[1]);
        return EAGAIN;
    }
}
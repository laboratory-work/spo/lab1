#include <stdio.h>
#include <string.h>
#include <blkid/blkid.h>
#include "../includes/include_main.h"

int part_1() {
    blkid_cache blkCache;

    if (blkid_get_cache(&blkCache, NULL) < 0) {
        fprintf(stderr, "Can't initialize blkid lib!\n");
        return -1;
    }

    if (blkid_probe_all(blkCache) < 0) {
        fprintf(stderr, "Can't probe devices!\n");
        return -1;
    }

    blkid_dev device;
    blkid_dev_iterate iterator = blkid_dev_iterate_begin(blkCache);

    int isRoot = 1;

    printf("Partitions\n");
    while (blkid_dev_next(iterator, &device) == 0) {
        const char *devName = blkid_dev_devname(device);
        blkid_probe probe = blkid_new_probe_from_filename(devName);
        if (probe == NULL) {
            fprintf(stdout, "\t%s\n", devName);
            fprintf(stderr, "Launch util as root to get more information!\n");
            isRoot = 0;
        }
        if (isRoot) {
            blkid_do_probe(probe);
            const char *uuid;
            blkid_probe_lookup_value(probe, "UUID", &uuid, NULL);
//            const char *label;
//            blkid_probe_lookup_value(probe, "LABEL", &label, NULL);
            const char *type;
            blkid_probe_lookup_value(probe, "TYPE", &type, NULL);
//            printf("\tName=%s, UUID=%s, LABEL=%s, TYPE=%s\n", devName, uuid, label, type);
            printf("\tName=%s, UUID=%s, TYPE=%s\n", devName, uuid, type);
        }
        blkid_free_probe(probe);
    }
    blkid_dev_iterate_end(iterator);
    return 0;
}
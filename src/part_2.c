#include <signal.h>
#include "../includes/include_main.h"
#include "../includes/include.h"
#include "../includes/part2_interact.h"

void interrupt_close() {
    clearFS();
    exit(1);
}

void interactive_test() {
    printCurrentDirs();
    copyToHost("t1.bin", "/home/lanolin/fs/t1.bin");
}

int part_2(char *filename) {
    signal(SIGINT, interrupt_close);
    openFS(filename);
    preSetup();
//    interactive_test();
    interactive_new();
    clearFS();
    return 0;
}
#include "../../includes/include.h"

//int get_reiserfs_format(struct reiserfs_super_block *sb) {
//    if (is_reiserfs_3_5_magic_string(sb) ||
//        (is_reiserfs_jr_magic_string(sb) && get_sb_version(sb) == REISERFS_FORMAT_3_5))
//        return REISERFS_FORMAT_3_5;
//
//    if (is_reiserfs_3_6_magic_string(sb) ||
//        (is_reiserfs_jr_magic_string(sb) && get_sb_version(sb) == REISERFS_FORMAT_3_6))
//        return REISERFS_FORMAT_3_6;
//
//    return REISERFS_FORMAT_UNKNOWN;
//}

char *key_of_what(const struct reiserfs_key *key) {
    switch (get_type(key)) {
        case TYPE_STAT_DATA:
            return "SD";
        case TYPE_INDIRECT:
            return "IND";
        case TYPE_DIRECT:
            return "DRCT";
        case TYPE_DIRENTRY:
            return "DIR";
        default:
            return "???";
    }
}

/*
 * old keys (on i386) have k_offset_v2.k_type == 15 (direct and indirect)
 * or == 0 (dir items and stat data)
 */
int is_key_format_1(int type) {
    return ((type == 0 || type == 15) ? 1 : 0);
}

int key_format(const struct reiserfs_key *key) {
    int type = get_key_type_v2(key);

    if (is_key_format_1(type))
        return KEY_FORMAT_1;

    return KEY_FORMAT_2;
}

size_t get_offset(const struct reiserfs_key *key) {
    if (key_format(key) == KEY_FORMAT_1)
        return get_key_offset_v1(key);

    return get_key_offset_v2(key);
}

int uniqueness2type(u_int32_t uniqueness) {
    switch (uniqueness) {
        case V1_SD_UNIQUENESS:
            return TYPE_STAT_DATA;
        case V1_INDIRECT_UNIQUENESS:
            return TYPE_INDIRECT;
        case V1_DIRECT_UNIQUENESS:
            return TYPE_DIRECT;
        case V1_DIRENTRY_UNIQUENESS:
            return TYPE_DIRENTRY;
        default:
            return TYPE_UNKNOWN;
    }
}

u_int32_t type2uniqueness(int type) {
    switch (type) {
        case TYPE_STAT_DATA:
            return V1_SD_UNIQUENESS;
        case TYPE_INDIRECT:
            return V1_INDIRECT_UNIQUENESS;
        case TYPE_DIRECT:
            return V1_DIRECT_UNIQUENESS;
        case TYPE_DIRENTRY:
            return V1_DIRENTRY_UNIQUENESS;
        default:
            return V1_UNKNOWN_UNIQUENESS;
    }
}

int get_type(const struct reiserfs_key *key) {
    int type_v2 = get_key_type_v2(key);

    if (is_key_format_1(type_v2))
        return uniqueness2type(get_key_uniqueness(key));

    return type_v2;
}

int who_is_this(const char *buf, int blocksize) {
    int res;

    /* super block? */
    if (does_look_like_super_block((void *) buf))
        return THE_SUPER;

    /* if block head and item head array seem matching
     * (node level, free space, item number, item locations and length),
     * then it is THE_LEAF, otherwise, it is HAS_IH_ARRAY
     */
    if ((res = is_a_leaf(buf, blocksize)))
        return res;

    if (is_correct_internal(buf, blocksize))
        return THE_INTERNAL;

    /* journal descriptor block? */
    if (is_desc_block(buf, blocksize))
        return THE_JDESC;

    /*
     * contents of buf does not look like reiserfs_1 metadata.
     * Bitmaps are possible here
     */
    return THE_UNKNOWN;
}

//int reiserfs_super_block_size(struct reiserfs_super_block *sb) {
//    switch (get_reiserfs_format(sb)) {
//        case REISERFS_FORMAT_3_5:
//            return SB_SIZE_V1;
//        case REISERFS_FORMAT_3_6:
//            return SB_SIZE;
//    }
//    fprintf(stderr, "Unknown format found");
//    return 0;
//}

int does_look_like_super_block(struct reiserfs_super_block *sb) {
    if (!is_any_reiserfs_magic_string(sb))
        return 0;

    if (!is_blocksize_correct(get_sb_block_size(sb)))
        return 0;

    return 1;
}

int is_desc_block(const char *buf, unsigned long buf_size) {
    const struct reiserfs_journal_desc *desc = (struct reiserfs_journal_desc *) buf;
    if (!memcmp(buf + buf_size - 12, JOURNAL_DESC_MAGIC, 8)
        && desc->j2_len > 0)
        return 1;
    return 0;
}

/* returns 1 if buf looks like an internal node, 0 otherwise */
int is_correct_internal(const char *buf, int blocksize) {
    const struct block_head *blkh;
    unsigned int nr;
    int used_space;

    blkh = (struct block_head *) buf;

    if (!is_internal_block_head(buf))
        return 0;

    nr = get_blkh_nr_items(blkh);
    /* for internal which is not root we might check min number of keys */
    if (nr > (blocksize - BLKH_SIZE - DC_SIZE) / (KEY_SIZE + DC_SIZE))
        return 0;

    used_space = BLKH_SIZE + KEY_SIZE * nr + DC_SIZE * (nr + 1);
    if (used_space != blocksize - get_blkh_free_space(blkh))
        return 0;

    return 1;
}

int leaf_blkh_correct(const char *buf, int blocksize) {
    struct block_head *blkh;
    unsigned int nr;

    blkh = (struct block_head *) buf;
    if (!is_leaf_block_head(buf))
        return 0;

    nr = get_blkh_nr_items(blkh);
    if (nr > ((blocksize - BLKH_SIZE) / (IH_SIZE + MIN_ITEM_LEN)))
        return 0;

    return leaf_free_space_estimate(buf, blocksize) == get_blkh_free_space(blkh);
}

int is_a_leaf(const char *buf, int blocksize) {
    const struct block_head *blkh;
    int counted;

    blkh = (struct block_head *) buf;
    if (!is_leaf_block_head(buf))
        return 0;

    counted = leaf_count_ih(buf, blocksize);

    /* if leaf block header is ok, check item count also. */
    if (leaf_blkh_correct(buf, blocksize))
        return counted >=
               get_blkh_nr_items(blkh) ? THE_LEAF : HAS_IH_ARRAY;

    /* leaf block header is corrupted, it is ih_array if some items were detected. */
    return counted ? HAS_IH_ARRAY : 0;
}

int is_reiserfs_3_5_magic_string(struct reiserfs_super_block *rs) {
    return (!strncmp(rs->s_v1.s_magic, REISERFS_3_5_SUPER_MAGIC_STRING, strlen(REISERFS_3_5_SUPER_MAGIC_STRING)));
}

int is_reiserfs_3_6_magic_string(struct reiserfs_super_block *rs) {
    return (!strncmp(rs->s_v1.s_magic, REISERFS_3_6_SUPER_MAGIC_STRING, strlen(REISERFS_3_6_SUPER_MAGIC_STRING)));
}

int is_reiserfs_jr_magic_string(struct reiserfs_super_block *rs) {
    return (!strncmp(rs->s_v1.s_magic, REISERFS_JR_SUPER_MAGIC_STRING, strlen(REISERFS_JR_SUPER_MAGIC_STRING)));
}

int is_any_reiserfs_magic_string(struct reiserfs_super_block *rs) {
    if (is_reiserfs_3_5_magic_string(rs) || is_reiserfs_3_6_magic_string(rs) || is_reiserfs_jr_magic_string(rs))
        return 1;
    return 0;
}

int is_blocksize_correct(unsigned int blocksize) {
    return ((((blocksize & -blocksize) == blocksize) && (blocksize >= 512) && (blocksize <= 8192)));
}

int leaf_free_space_estimate(const char *buf, int blocksize) {
    const struct block_head *blkh;
    const struct item_head *ih;
    int nr;

    blkh = (struct block_head *) buf;
    nr = get_blkh_nr_items(blkh);
    ih = (struct item_head *) (buf + BLKH_SIZE) + nr - 1;

    if (nr) {
        return get_ih_location(ih) - BLKH_SIZE - IH_SIZE * nr;
    } else {
        return blocksize - BLKH_SIZE - IH_SIZE * nr;
    }
}

int leaf_count_ih(const char *buf, int blocksize) {
    const struct item_head *ih;
    int prev_location;
    int nr;

    /* look at the table of item head */
    prev_location = blocksize;
    ih = (struct item_head *) (buf + BLKH_SIZE);
    nr = 0;
    while (1) {
        if (get_ih_location(ih) + get_ih_item_len(ih) != prev_location)
            break;
        if (get_ih_location(ih) < IH_SIZE * (nr + 1) + BLKH_SIZE)
            break;
        if (get_ih_item_len(ih) > MAX_ITEM_LEN(blocksize))
            break;
        prev_location = get_ih_location(ih);
        ih++;
        nr++;
    }

    return nr;
}

int leaf_item_number_estimate(const char *bh, int blocksize) {
    const struct block_head *blkh = (const struct block_head *) bh;
    int nr = leaf_count_ih(bh, blocksize);
    return nr >= get_blkh_nr_items(blkh) ? get_blkh_nr_items(blkh) : nr;
}
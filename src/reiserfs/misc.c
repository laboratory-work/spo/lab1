#include "../../includes/include.h"

struct read_blocks {
    size_t block_nr;
    char *block;
};

struct read_blocks **rBlocks;
size_t capacity;
int indexRBlocks = 0;

struct read_blocks *find_in_read_blocks_on_number(size_t block_numb) {
    for(int i = 0; i < capacity; i++) {
        if(rBlocks[i] && rBlocks[i]->block && rBlocks[i]->block_nr == block_numb)
            return rBlocks[i];
    }
    return NULL;
}

void init_read_block(size_t capacity_1) {
    capacity = capacity_1;
    rBlocks = calloc_int(capacity, sizeof(struct read_blocks *));
}

void *block_read_data(FILE *fp, size_t block_count, size_t block_size) {
    void *buffer = calloc_int(1, block_size);
    size_t shift = block_count * block_size;
    fseek(fp, shift, SEEK_SET);
    fread(buffer, block_size, 1, fp);
    return buffer;
}

void *block_read(FILE *fp, size_t block_count, size_t block_size) {
    struct read_blocks *rb = find_in_read_blocks_on_number(block_count);
    if(rb) return rb->block;
    void *buffer = block_read_data(fp, block_count, block_size);
    if(rBlocks) {
        rBlocks[indexRBlocks] = calloc_int(1, sizeof(struct read_blocks));
        rBlocks[indexRBlocks]->block_nr = block_count;
        rBlocks[indexRBlocks]->block = buffer;
        indexRBlocks++;
    }
    return buffer;
}

void free_read_block() {
    for(int i = 0; i < capacity; i++) {
        if(rBlocks[i]) {
            if (rBlocks[i]->block)
                free_int(rBlocks[i]->block);
            free_int(rBlocks[i]);
        }
    }
    free_int(rBlocks);
}
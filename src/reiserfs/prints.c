#include "../../includes/prints.h"

void printDirs(struct directories *dir, int level) {
    if (dir) {
        if (dir->current) {
            for (int i = 0; i < level; i++) {
                fprintf(stdout, "\t");
            }

            if (dir->stat_v2) {
                fprintf(stdout,
                        "D=%d O=%d Mode=%xd UID=%d GID=%d SSS=%lu IND=%zu DU=%zu %s\n",
                        dir->current->k2_dir_id,
                        dir->current->k2_objectid,
                        dir->stat_v2->sd_mode,
                        dir->stat_v2->sd_uid,
                        dir->stat_v2->sd_gid,
                        dir->stat_v2->sd_size,
                        (size_t) dir->indirect,
                        (size_t) dir->direct,
                        dir->name
                );
            } else if (dir->stat_v1) {
                fprintf(stdout,
                        "D=%d O=%d Mode=%xd UID=%d GID=%d SSS=%u IND=%zu DU=%zu %s\n",
                        dir->current->k2_dir_id,
                        dir->current->k2_objectid,
                        dir->stat_v1->sd_mode,
                        dir->stat_v1->sd_uid,
                        dir->stat_v1->sd_gid,
                        dir->stat_v1->sd_size,
                        (size_t) dir->indirect,
                        (size_t) dir->direct,
                        dir->name
                );
            } else {
                fprintf(stdout,
                        "D=%d O=%d %s\n",
                        dir->current->k2_dir_id,
                        dir->current->k2_objectid,
                        dir->name
                );
            }
        }
        for (int j = 0; j < dir->countChild; j++) {
            printDirs(dir->child[j], level + 1);
        }
    }
}


char *typeFile2name(int mode) {
    if ((mode & S_IFSOCK) == S_IFSOCK) {
        return "s";
    } else if ((mode & S_IFLNK) == S_IFLNK) {
        return "l";
    } else if ((mode & S_IFREG) == S_IFREG) {
        return "-";
    } else if ((mode & S_IFBLK) == S_IFBLK) {
        return "b";
    } else if ((mode & S_IFDIR) == S_IFDIR) {
        return "d";
    } else if ((mode & S_IFCHR) == S_IFCHR) {
        return "c";
    } else if ((mode & S_IFIFO) == S_IFIFO) {
        return "f";
    } else
        return "";
}

void printSuperBlock(struct reiserfs_super_block *superBlock) {
    fprintf(stdout, "******* Super block *********\n");
    fprintf(stdout, "Block count: %d\n", superBlock->s_v1.sb_block_count);
    fprintf(stdout, "Free blocks: %d\n", superBlock->s_v1.sb_free_blocks);
    fprintf(stdout, "Root block: %d\n", superBlock->s_v1.sb_root_block);
    fprintf(stdout, "Journal block: %d\n", superBlock->s_v1.sb_journal.jp_journal_1st_block);
    fprintf(stdout, "Journal device: %d\n", superBlock->s_v1.sb_journal.jp_journal_dev);
    fprintf(stdout, "Original journal size: %d\n", superBlock->s_v1.sb_journal.jp_journal_size);
    fprintf(stdout, "Journal trans. max: %d\n", superBlock->s_v1.sb_journal.jp_journal_trans_max);
    fprintf(stdout, "Journal magic: %d\n", superBlock->s_v1.sb_journal.jp_journal_magic);
    fprintf(stdout, "Journal max. batch: %d\n", superBlock->s_v1.sb_journal.jp_journal_max_batch);
    fprintf(stdout, "Journal max. commit age: %d\n", superBlock->s_v1.sb_journal.jp_journal_max_commit_age);
    fprintf(stdout, "Journal max. trans. age: %d\n", superBlock->s_v1.sb_journal.jp_journal_max_trans_age);
    fprintf(stdout, "Reserved for journal: %d\n", superBlock->s_v1.sb_reserved_for_journal);
    fprintf(stdout, "Blocksize: %d\n", superBlock->s_v1.sb_blocksize);
    fprintf(stdout, "OID max. size: %d\n", superBlock->s_v1.sb_oid_maxsize);
    fprintf(stdout, "OID current size: %d\n", superBlock->s_v1.sb_oid_cursize);
    fprintf(stdout, "State: %d\n", superBlock->s_v1.sb_fs_state);
    fprintf(stdout, "Magic String: %s\n", superBlock->s_v1.s_magic);
    fprintf(stdout, "Hash function code: %d\n", superBlock->s_v1.sb_hash_function_code);
    fprintf(stdout, "Tree height: %d\n", superBlock->s_v1.sb_tree_height);
    fprintf(stdout, "Bitmap number: %d\n", superBlock->s_v1.sb_bmap_nr);
    fprintf(stdout, "Version: %d\n", superBlock->s_v1.sb_version);
    fprintf(stdout, "Umount state: %d\n", superBlock->s_v1.sb_umount_state);
    fprintf(stdout, "Check interval: %d\n", superBlock->s_check_interval);
    fprintf(stdout, "Flags: %d\n", superBlock->s_flags);
    fprintf(stdout, "Label: %s\n", superBlock->s_label);
    fprintf(stdout, "Last Check: %d\n", superBlock->s_lastcheck);
    fprintf(stdout, "Max mnt Count: %d\n", superBlock->s_max_mnt_count);
    fprintf(stdout, "Mnt Count: %d\n", superBlock->s_mnt_count);
    fprintf(stdout, "Unused: %s\n", superBlock->s_unused);
    fprintf(stdout, "UUID: %s\n", superBlock->s_uuid);
    fprintf(stdout, "Inode Generation: %d\n", superBlock->sb_inode_generation);
    fprintf(stdout, "****************\n");
}

void printKey(struct reiserfs_key *key) {
    fprintf(stdout, "******* Key *********\n");
    fprintf(stdout, "Dir ID: %d\n", key->k2_dir_id);
    fprintf(stdout, "Object ID: %d\n", key->k2_objectid);
    fprintf(stdout, "Offset: %zu\n", get_offset(key));
    fprintf(stdout, "Type: %s\n", key_of_what(key));
    fprintf(stdout, "****************\n");
}

void printBlocHead(struct block_head *bh) {
    fprintf(stdout, "******* Block Head *********\n");
    fprintf(stdout, "Level: %d\n", bh->blk2_level);
    fprintf(stdout, "Count of items: %d\n", bh->blk2_nr_item);
    fprintf(stdout, "Free space: %d\n", bh->blk2_free_space);
    fprintf(stdout, "Reserved: %d\n", bh->blk_reserved);
    fprintf(stdout, "****************\n");
}

void printDiskChild(struct disk_child *dc) {
    fprintf(stdout, "******* Disk Child *********\n");
    fprintf(stdout, "Block Number: %d\n", dc->dc2_block_number);
    fprintf(stdout, "Size: %d\n", dc->dc2_size);
    fprintf(stdout, "****************\n");
}

void printItemHead(struct item_head *ih) {
    fprintf(stdout, "----- Item Head ----\n");
    printKey(&(ih->ih_key));
    fprintf(stdout, "Entry Count: %d\n", ih->u.ih2_entry_count);
    fprintf(stdout, "Free spaces: %d\n", ih->u.ih2_free_space);
    fprintf(stdout, "Item len: %d\n", ih->ih2_item_len);
    fprintf(stdout, "Item location: %d\n", ih->ih2_item_location);
    fprintf(stdout, "Item version: %d\n", ih->ih_format);
    fprintf(stdout, "----------\n");
}

void printStatData1(struct stat_data_v1 *sd1) {
    fprintf(stdout, "----- Stat Data 1 ----\n");
    fprintf(stdout, "Mode: %d\n", sd1->sd_mode);
    fprintf(stdout, "Num links: %d\n", sd1->sd_nlink);
    fprintf(stdout, "UID: %d\n", sd1->sd_uid);
    fprintf(stdout, "GID: %d\n", sd1->sd_gid);
    fprintf(stdout, "Size: %d\n", sd1->sd_size);
    fprintf(stdout, "Atime: %d\n", sd1->sd_atime);
    fprintf(stdout, "Mtime: %d\n", sd1->sd_mtime);
    fprintf(stdout, "Ctime: %d\n", sd1->sd_ctime);
    fprintf(stdout, "Rdev/blocks: %d\n", sd1->u.sd_blocks);
    fprintf(stdout, "First dir. byte: %d\n", sd1->sd_first_direct_byte);
    fprintf(stdout, "-----  ----\n");
}

void printStatData(struct stat_data *sd) {
    fprintf(stdout, "----- Stat Data ----\n");
    fprintf(stdout, "Mode: %xd\n", sd->sd_mode);
    fprintf(stdout, "Reserved: %d\n", sd->sd_attrs);
    fprintf(stdout, "Num links: %d\n", sd->sd_nlink);
    fprintf(stdout, "Size: %lu\n", sd->sd_size);
    fprintf(stdout, "UID: %d\n", sd->sd_uid);
    fprintf(stdout, "GID: %d\n", sd->sd_gid);
    fprintf(stdout, "Atime: %d\n", sd->sd_atime);
    fprintf(stdout, "Mtime: %d\n", sd->sd_mtime);
    fprintf(stdout, "Ctime: %d\n", sd->sd_ctime);
    fprintf(stdout, "Blocks: %d\n", sd->sd_blocks);
    fprintf(stdout, "Rdev/blocks: %d\n", sd->u.sd_rdev);
    fprintf(stdout, "-----  ----\n");
}

void printDirHead(struct reiserfs_de_head *de) {
    fprintf(stdout, "----- Dir Head ----\n");
    fprintf(stdout, "Offset: %d\n", de->deh2_offset);
    fprintf(stdout, "Dir ID: %d\n", de->deh2_dir_id);
    fprintf(stdout, "Obj ID: %d\n", de->deh2_objectid);
    fprintf(stdout, "Loc: %d\n", de->deh2_location);
    fprintf(stdout, "State: %d\n", de->deh2_state);
    fprintf(stdout, "-----  ----\n");
}
#include "../../includes/stack.h"

// function to create a stack of given capacity. It initializes size of
// stack as 0
struct stack *createStack(unsigned capacity) {
    struct stack *stack = (struct stack *) malloc(sizeof(struct stack));
    stack->capacity = capacity;
    stack->top = -1;
    stack->array = malloc(stack->capacity * sizeof(int));
    return stack;
}

// Stack is full when top is equal to the last index
int isFull(struct stack *stack) {
    return stack->top == stack->capacity - 1;
}

// stack is empty when top is equal to -1
int isEmpty(struct stack *stack) {
    return stack->top == -1;
}

// Function to add an item to stack.  It increases top by 1
void push(struct stack *stack, int item) {
    if (isFull(stack))
        return;
    stack->array[++stack->top] = item;
}

// Function to remove an item from stack.  It decreases top by 1
int pop(struct stack *stack) {
    if (isEmpty(stack))
        return -1;
    return stack->array[stack->top--];
}

// Function to return the top from stack without removing it
int peek(struct stack *stack) {
    if (isEmpty(stack))
        return -1;
    return stack->array[stack->top];
}

void clean(struct stack *stack) {
    free(stack->array);
    free(stack);
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ftw.h>
#include <errno.h>
#include <limits.h>
#include "../../includes/include.h"
#include "../../includes/stack.h"
#include "../../includes/prints.h"

struct items {
    struct item_head *head;
    char *item;
};

FILE *fp;

int blocksize = DEFAULT_BLOCKSIZE;
int readBlocks;
u_int32_t capacity;

struct reiserfs_super_block *reiserfsSuperBlock;
struct items *allItems;

struct directories *root;
struct reiserfs_key root_dir_key = {.k2_dir_id=REISERFS_ROOT_PARENT_OBJECTID, .k2_objectid=REISERFS_ROOT_OBJECTID};
struct reiserfs_key *current_dir_key = &root_dir_key;
struct reiserfs_key up_root_dir_key = {.k2_dir_id=REISERFS_ROOT_PARENT_OBJECTID - 1, .k2_objectid=
REISERFS_ROOT_OBJECTID - 1};


struct directories *find_dir_on_key(struct directories *head, struct reiserfs_key *key) {
    if (head && head->current) {
        if (cmp_keys(head->current, key)) {
            return head;
        }
        for (int i = 0; i < head->countChild; i++) {
            struct directories *f = find_dir_on_key(head->child[i], key);
            if (f) {
                return f;
            }
        }
    }
    return NULL;
}

struct directories *find_dir_on_name(struct directories *head, char *name) {
    if (head && head->current) {
        if (strcmp(head->name, name) == 0) {
            return head;
        }
        for (int i = 0; i < head->countChild; i++) {
            struct directories *f = find_dir_on_name(head->child[i], name);
            if (f) {
                return f;
            }
        }
    }
    return NULL;
}

int cmp_keys(struct reiserfs_key *key1, struct reiserfs_key *key2) {
    return (key1->k2_dir_id == key2->k2_dir_id && key1->k2_objectid == key2->k2_objectid);
}

void set_stat_data(struct directories *head, const char *item, int subtype) {
    if (subtype == KEY_FORMAT_2) {
        head->stat_v2 = calloc_int(1, SD_SIZE);
        memcpy(head->stat_v2, (struct stat_data *) (item), SD_SIZE);
    } else if (subtype == KEY_FORMAT_1) {
        head->stat_v1 = calloc_int(1, SD_V1_SIZE);
        memcpy(head->stat_v1, (struct stat_data_v1 *) (item), SD_V1_SIZE);
    }
}

void fill_inderect(struct directories *head, const struct reiserfs_key *key,
                   const struct indirect_node *pNode, size_t nmemb) {
    head->indirect->blocks = realloc_int(head->indirect->blocks, (head->indirect->count + 1) * sizeof(u_int32_t *));
    head->indirect->blocks[head->indirect->count] = calloc_int(nmemb, sizeof(char));
    memcpy(head->indirect->blocks[head->indirect->count], (char *) pNode->blocks, nmemb);
    head->indirect->offsets = realloc_int(head->indirect->offsets, (head->indirect->count + 1) * sizeof(size_t));
    head->indirect->offsets[head->indirect->count] = get_offset(key);
    head->indirect->len_in_block = realloc_int(head->indirect->len_in_block,
                                               (head->indirect->count + 1) * sizeof(size_t));
    head->indirect->len_in_block[head->indirect->count] = nmemb;
    head->indirect->count++;
}

void create_inderect(struct directories *head, const struct reiserfs_key *key, int length,
                     const struct indirect_node *pNode, size_t nmemb) {
    head->indirect = calloc_int(1, INDIRNOD_SIZE);
    head->indirect->count = length;
    head->indirect->blocks = calloc_int(1, sizeof(u_int32_t *));
    head->indirect->len_in_block = calloc_int(1, sizeof(u_int32_t *));
    head->indirect->offsets = calloc_int(1, sizeof(size_t));
    head->indirect->blocks[0] = calloc_int(nmemb, sizeof(char));
    memcpy(head->indirect->blocks[0], (char *) pNode->blocks, nmemb);
    head->indirect->offsets[0] = get_offset(key);
    head->indirect->len_in_block[0] = nmemb;
}

void set_inderect_data(struct directories *head, const struct reiserfs_key *key, const char *item, int length) {
    struct indirect_node *pNode = (struct indirect_node *) item;
    size_t nmemb = (size_t) pNode->len_in_block;
    if (head->indirect) {
        fill_inderect(head, key, pNode, nmemb);
    } else {
        create_inderect(head, key, length, pNode, nmemb);
    }
    head->length += length;
}

void set_direct_data(struct directories *head, const struct reiserfs_key *key, const char *item, int length) {
    size_t offset = get_offset(key);
    if (head->direct) {
        if (offset > head->direct->capacity) {
            head->direct->block = realloc_int(head->direct->block, head->direct->capacity + offset + length);
            head->direct->capacity += offset + length;
        }
        head->direct->length += length;
    } else {
        head->direct = calloc_int(1, DIRNOD_SIZE);
        head->direct->block = calloc_int(offset + length, sizeof(char));
        head->direct->capacity = offset + length;
        head->direct->length = length;
    }
    memcpy(head->direct->block + offset - 1, item, length);
    head->length += length;
}

void set_all_data(struct directories *head, struct reiserfs_key *key, char *item, int length, int type, int subtype) {
    if (head) {
        if (cmp_keys(head->current, key)) {
            switch (type) {
                case TYPE_STAT_DATA:
                    set_stat_data(head, item, subtype);
                    break;
                case TYPE_INDIRECT:
                    set_inderect_data(head, key, item, length);
                    break;
                case TYPE_DIRECT:
                    set_direct_data(head, key, item, length);
                    break;
                default:
                    break;
            }
        }
        for (int i = 0; i < head->countChild; i++) {
            set_all_data(head->child[i], key, item, length, type, subtype);
        }
    }
}

void openFS(char *file_name) {
    fp = fopen(file_name, "rb");
    if(fp == NULL) {
        fprintf(stderr, "Failed open. Try to open on root.");
        exit(1);
    }
    size_t block_count_sb = REISERFS_DISK_OFFSET_IN_BYTES / blocksize;
    reiserfsSuperBlock = (struct reiserfs_super_block *) block_read(fp, block_count_sb, blocksize);
    if (who_is_this((const char *) reiserfsSuperBlock, blocksize) != THE_SUPER) {
        die("Ooops. This is not ReiserFS !!!");
    }
    if (!is_blocksize_correct(reiserfsSuperBlock->s_v1.sb_blocksize)) {
        die("Incorrect block size: %i. Expected: %i", reiserfsSuperBlock->s_v1.sb_blocksize, blocksize);
    }
    blocksize = reiserfsSuperBlock->s_v1.sb_blocksize;
    capacity = reiserfsSuperBlock->s_v1.sb_block_count - reiserfsSuperBlock->s_v1.sb_free_blocks + 1;
}

void readAllFS() {
    init_read_block(capacity);
    readBlocks = 0;
    allItems = calloc(capacity, ITM_SIZE);
    struct stack *queue = createStack(capacity);
    push(queue, reiserfsSuperBlock->s_v1.sb_root_block);
    while (!isEmpty(queue)) {
        int current = pop(queue);
        char *block = block_read(fp, current, blocksize);
        switch (who_is_this(block, blocksize)) {
            case THE_INTERNAL: {
                int nr_item = ((struct block_head *) block)->blk2_nr_item;
                for (int j = 0; j < nr_item; j++) {
                    struct disk_child *pointer = (struct disk_child *) (block + BLKH_SIZE + KEY_SIZE * nr_item) + j;
                    push(queue, pointer->dc2_block_number);
                }
                block = NULL;
                free_int(block);
                break;
            }
            case THE_LEAF: {
                int nr_item = leaf_item_number_estimate(block, blocksize);
                for (int j = 0; j < nr_item; j++) {
                    struct item_head *ih = (struct item_head *) (block + BLKH_SIZE) + j;
                    allItems[readBlocks].head = ih;
                    allItems[readBlocks].item = block + ih->ih2_item_location;
                    readBlocks++;
                }
                break;
            }
            default:
                break;
        }
    }
    clean(queue);
}

void init_dir_struct_root() {
    root = calloc_int(1, DIR_SIZE);
    root->current = calloc_int(1, KEY_SIZE);
    memcpy(root->current, &root_dir_key, KEY_SIZE);
    root->name = calloc_int(1, sizeof(char));
    strcpy(root->name, "/");
    set_key_uniqueness(root->current, type2uniqueness(TYPE_DIRENTRY));
}

void initDirs() {
    init_dir_struct_root();
    for (int k = 0; k < readBlocks; k++) {
        struct items it = allItems[k];
        int type = get_type(&(it.head->ih_key));
        if (type == TYPE_DIRENTRY) {
            struct directories *dir = find_dir_on_key(root, &(it.head->ih_key));
            if (dir == NULL) { continue; }
            int entryCount = it.head->u.ih2_entry_count;
            dir->child = calloc_int(entryCount, DIR_SIZE);
            dir->countChild = entryCount;
            for (int i = 0; i < entryCount; i++) {
                dir->child[i] = calloc_int(1, DIR_SIZE);
                struct reiserfs_de_head *dir_head = (struct reiserfs_de_head *) it.item + i;
                char *name = it.item + dir_head->deh2_location;
                int size = 1;
                while (*(name + size) != '\0') { size++; }
                dir->child[i]->name = calloc_int(size, sizeof(char));
                memcpy(dir->child[i]->name, name, size);
                dir->child[i]->current = calloc_int(1, KEY_SIZE);
                dir->child[i]->current->k2_dir_id = dir_head->deh2_dir_id;
                dir->child[i]->current->k2_objectid = dir_head->deh2_objectid;
            }
        }
    }
}

void init_data() {
    for (int k = 0; k < readBlocks; k++) {
        struct items it = allItems[k];
        int type = get_type(&(it.head->ih_key));

        if (type == TYPE_STAT_DATA) {
            set_all_data(root, &(it.head->ih_key), it.item, 0, TYPE_STAT_DATA, it.head->ih_format);
        } else if (type == TYPE_INDIRECT) {
            struct indirect_node *indr = calloc_int(1, INDIRNOD_SIZE);
            indr->count = 1;
            indr->blocks = (u_int32_t **) it.item;
            indr->len_in_block = (u_int32_t *) ((u_int32_t) it.head->ih2_item_len);
            set_all_data(root, &(it.head->ih_key), (char *) indr, 1, TYPE_INDIRECT, 0);
            indr->len_in_block = NULL;
            indr->blocks = NULL;
            free_int(indr);
        } else if (type == TYPE_DIRECT) {
            set_all_data(root, &(it.head->ih_key), it.item, it.head->ih2_item_len, TYPE_DIRECT, 0);
        } else if (type == TYPE_DIRENTRY) {
            continue;
        } else {
            fprintf(stderr, "Unknowns\n");
        }
    }
}

void install_is_dir(struct directories *head) {
    if (head->stat_v2) {
        if ((head->stat_v2->sd_mode & S_IFDIR) == S_IFDIR) {
            set_key_uniqueness(head->current, type2uniqueness(TYPE_DIRENTRY));
        }
    } else if (head->stat_v1) {
        if ((head->stat_v1->sd_mode & S_IFDIR) == S_IFDIR) {
            set_key_uniqueness(head->current, type2uniqueness(TYPE_DIRENTRY));
        }
    }

    if (head->countChild > 0 && head->child) {
        for (int i = 0; i < head->countChild; i++) {
            install_is_dir(head->child[i]);
        }
    }
}

void preSetup() {
    readAllFS();
    initDirs();
    init_data();
    install_is_dir(root);
}

void clearDirectories(struct directories *head);

void clearFS() {
    free_int(allItems);
    clearDirectories(root);
    free_read_block();
    free_int(reiserfsSuperBlock);
    fclose(fp);
}

void clearIndirectStruct(struct indirect_node *in) {
    if (in) {
        for (int i = 0; i < in->count; i++) {
            if (in->blocks[i]) free_int(in->blocks[i]);
        }
        if (in->len_in_block) free_int(in->len_in_block);
        if (in->offsets) free_int(in->offsets);
        free_int(in);
    }
}

void clearDirectStruct(struct direct_node *dn) {
    if (dn) {
        if (dn->block) free_int(dn->block);
        free_int(dn);
    }
}

void clearDirectories(struct directories *head) {
    if (head) {
        for (int i = 0; i < head->countChild; i++) {
            clearDirectories(head->child[i]);
        }
        if (head->child) free_int(head->child);
        clearIndirectStruct(head->indirect);
        clearDirectStruct(head->direct);
        if (head->current) free_int(head->current);
        if (head->name) free_int(head->name);
        if (head->stat_v2) free_int(head->stat_v2);
        if (head->stat_v1) free_int(head->stat_v1);
    }
}

void printCurrentDirs() {
    struct directories *dirOnKey = find_dir_on_key(root, current_dir_key);
    if (dirOnKey) {
        for (int i = 0; i < dirOnKey->countChild; i++) {
            struct directories *child = dirOnKey->child[i];
            if (child->stat_v2) {
                fprintf(stdout, "%s%o\t%s\n",
                        typeFile2name(child->stat_v2->sd_mode),
                        (child->stat_v2->sd_mode & S_MODE), child->name
                );
            } else {
                fprintf(stdout, "d000\t%s\n", child->name);
            }
        }

    } else {
        die("Errors on print");
    }
}

void moveDirectory(char *new) {
    struct directories *currentdir = find_dir_on_key(root, current_dir_key);
    struct directories *dirOnName = find_dir_on_name(currentdir, new);
    if (dirOnName) {
        if (cmp_keys(dirOnName->current, &up_root_dir_key)) {
            fprintf(stderr, "Sorry, but this level is undefined\n");
            return;
        }
        if ((dirOnName->stat_v2->sd_mode & S_IFDIR) == S_IFDIR) {
            current_dir_key = dirOnName->current;
        } else {
            fprintf(stderr, "Is not a directory\n");
        }
    } else {
        fprintf(stderr, "Not found\n");
    }
}

void copyFileToHost(struct directories *dirOnName, char *fn) {
    FILE *n = fopen(fn, "wb+");
    if (n) {
        if (dirOnName->length > 0 && dirOnName->direct) {
            fwrite(dirOnName->direct->block, dirOnName->direct->length, 1, n);
        } else if (dirOnName->length > 0 && dirOnName->indirect) {
            for (int i = 0; i < dirOnName->indirect->count; i++) {
                u_int32_t *link = dirOnName->indirect->blocks[i];
                size_t count = dirOnName->indirect->len_in_block[i] / sizeof(u_int32_t);
                fseek(n, (size_t) (dirOnName->indirect->offsets[i] - 1), SEEK_SET);
                for (int j = 0; j < count; j++) {
                    u_int32_t data_block_ind = *(link + j);
                    char *data_block = block_read(fp, data_block_ind, blocksize);
                    fwrite(data_block, sizeof(char), blocksize, n);
                    data_block = NULL;
                    free_int(data_block);
                }
                fflush(n);
            }
        }
        fflush(n);
        fclose(n);
    } else {
        fprintf(stderr, "Cannot create file %s\n", fn);
    }
}

void copyDirectoryToHost(struct directories *dirOnName, char *hostDir) {
    int result = mkdir(hostDir, 0755);
    if (result == -1 && errno != EEXIST) {
        fprintf(stderr, "Have error on copying file");
        return;
    }

    for (int i = 0; i < dirOnName->countChild; i++) {
        struct directories *d = dirOnName->child[i];
        if (strcmp(d->name, ".") == 0 || strcmp(d->name, "..") == 0) {
            continue;
        }
        char *newName = calloc_int(PATH_MAX, sizeof(char));
        snprintf(newName, PATH_MAX, "%s/%s", hostDir, d->name);
        if (is_direntry_key(d->current)) {
            copyDirectoryToHost(d, newName);
        } else {
            copyFileToHost(d, newName);
        }
        free_int(newName);
    }

}

void copyToHost(char *name, char *host) {
    struct directories *dirOnName;
    if(name[0] == '/') {
        struct directories *currentdir = find_dir_on_key(root, &root_dir_key);
        int elem = -1;
        char **paths = str_split(name, '/', &elem);
        for(int i = 0; i < elem; i++) {
            currentdir = find_dir_on_name(currentdir, paths[i]);
        }
        free_int(paths);
        dirOnName = currentdir;
    }else{
        struct directories *currentdir = find_dir_on_key(root, current_dir_key);
        dirOnName = find_dir_on_name(currentdir, name);
    }

    if (dirOnName) {
        if (is_direntry_key(dirOnName->current) && dirOnName->countChild > 0 && dirOnName->child) {
            copyDirectoryToHost(dirOnName, host);
        } else if (!is_direntry_key(dirOnName->current) && !dirOnName->child) {
            copyFileToHost(dirOnName, host);
        }
    } else {
        fprintf(stderr, "Not found or it is directory\n");
    }

}

char *getCurrentDirName() {
    struct directories *curr = find_dir_on_key(root, current_dir_key);
    if (curr == NULL) die("ERROR!!!! Internal. Not found name in key");
    return curr->name;
}
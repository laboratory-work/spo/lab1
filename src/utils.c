#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/utils.h"

void die(const char *fmt, ...) {
    static char buf[1024];
    va_list args;

    va_start(args, fmt);
    vsprintf(buf, fmt, args);
    va_end(args);

    fprintf(stderr, "\n%s\n", buf);
    exit(1);
    //    abort();
}

void *calloc_int(size_t nmemb, size_t size) {
    void *calloc1 = calloc(nmemb, size);
    if(calloc1 == NULL) {
        die("ERROR!!!! Cannot init calloc memory");
    }
    return calloc1;
}

void free_int(void *ptr) {
    if(ptr) {
        ptr = NULL;
        free(ptr);
    }
}

void *realloc_int(void *ptr, size_t size) {
    void *pVoid = realloc(ptr, size);
    if(pVoid == NULL) {
        die("ERROR!!!! Cannot realloc memory");
    }
    return pVoid;
}

char **str_split(char *a_str, const char a_delim, int *countElement) {
    char **result;
    size_t count = 0;
    char *tmp = a_str;
    char *last_comma = 0;
    char delim[2] = { a_delim, 0 };

    while (*tmp) {
        if (a_delim == *tmp) {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    count += last_comma < (a_str + strlen(a_str) - 1);
    count++;

    result = calloc_int(count, sizeof(char *));

    if (result) {
        size_t idx = 0;
        char *token = strtok(a_str, delim);

        while (token) {
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        *(result + idx) = 0;
        if(countElement) {
            *countElement = idx;
        }
    }

    return result;
}

char *ltrim(char *str, const char *sep) {
    if(sep == NULL) {
        sep = "\t\n\v\f\r ";
    }
    size_t totrim = strspn(str, sep);
    if(totrim > 0){
        size_t len = strlen(str);
        if(totrim == len) {
            str[0] = '\0';
        }else{
            memmove(str, str+totrim, len+1-totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *sep) {
    if(sep == NULL) {
        sep = "\t\n\v\f\r ";
    }
    size_t i = strlen(str) - 1;
    while (i >= 0 && strchr(sep, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}

char *trim(char *str) {
    return ltrim(rtrim(str, NULL), NULL);
}

char *trim1(char *str, const char *sep) {
    return ltrim(rtrim(str, sep), sep);
}